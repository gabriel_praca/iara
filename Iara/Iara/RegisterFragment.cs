using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Text.RegularExpressions;

namespace Iara
{
    class RegisterFragment : DialogFragment
    {
        private Button btnRegister;
        private EditText edtEmail;
        private EditText edtUser;
        private EditText edtPass;
        private EditText edtConfirmPass;
        private string emailRule = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.RegisterFragment, container, false);

            edtEmail = view.FindViewById<EditText>(Resource.Id.edtEmail);
            edtUser = view.FindViewById<EditText>(Resource.Id.edtUser);
            edtPass = view.FindViewById<EditText>(Resource.Id.edtPassword);
            edtConfirmPass = view.FindViewById<EditText>(Resource.Id.edtConfirmPassword);
            btnRegister = view.FindViewById<Button>(Resource.Id.btnRegister);
            btnRegister.Click += btnRegister_Click;

            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            CreateUser();
        }

        private void CreateUser()
        {
            if(!Regex.IsMatch(edtEmail.Text, emailRule))
            {
                Toast.MakeText(Activity.ApplicationContext, "Email Inv�lido", ToastLength.Short).Show();
            }

            else if(edtUser.Text == String.Empty || edtPass.Text == String.Empty)
            {
                Toast.MakeText(Activity.ApplicationContext, "Todos os campos s�o obrigat�rios", ToastLength.Short).Show();
            }

            else if(edtPass.Text == edtConfirmPass.Text)
            {
                SQLiteModels.User user = new SQLiteModels.User();

                user.email = edtEmail.Text;
                user.userName = edtUser.Text;
                user.password = edtPass.Text;
                user.updtDTime = DateTime.Now;

                if(DatabaseManager.BODatabaseManager.CreateUser(user) != DatabaseManager.DatabaseAnswer.Sucess.ToString())
                {
                    Toast.MakeText(Activity.ApplicationContext, "Usu�rio j� existe", ToastLength.Short).Show();
                }
                else
                {
                    Toast.MakeText(Activity.ApplicationContext, "Registrado com sucesso", ToastLength.Short).Show();
                }
                //TEMP
                //SQLiteModels.User retUser = DatabaseManager.BODatabaseManager.GetUser(user.email);
                //Toast.MakeText(Activity.ApplicationContext, String.Concat(retUser.userName, " | ", retUser.email, " | ", retUser.password, " | ", retUser.updtDTime), ToastLength.Short).Show();
                
            }
            else
            {
                Toast.MakeText(Activity.ApplicationContext, "Confirma��o da senha inv�lida", ToastLength.Short).Show();
            }
        }
    }
}