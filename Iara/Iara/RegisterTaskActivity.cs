using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Icu.Util;

namespace Iara
{
    [Activity(Label = "Registre uma Tarefa", ParentActivity = typeof(RootActivity),
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]

    public class RegisterTaskActivity : Activity
    {
        LinearLayout componentWeek;
        LinearLayout componentMonth;
        
        Button btnWeek;
        Button btnMonth;
        Button btnSaveTask;

        TextView txtTime;
        TextView txtMonth;

        EditText edtDesc;

        CheckBox ckSun;
        CheckBox ckMon;
        CheckBox ckTue;
        CheckBox ckWed;
        CheckBox ckThu;
        CheckBox ckFri;
        CheckBox ckSat;
        CheckBox ckRep;

        DateTime dateSelected;
        TimeSpan timeSelected;

        eTaskType actualTaskType = eTaskType.week;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.RegisterTask);

            componentMonth = (LinearLayout)FindViewById(Resource.Id.ComponentMonth);
            componentWeek = (LinearLayout)FindViewById(Resource.Id.ComponentWeek);

            componentWeek.Visibility = ViewStates.Visible;

            edtDesc = (EditText)FindViewById(Resource.Id.edtDesc);

            ckRep = (CheckBox)FindViewById(Resource.Id.ckRep);

            ckSun = (CheckBox)FindViewById(Resource.Id.ckSun);
            ckMon = (CheckBox)FindViewById(Resource.Id.ckMon);
            ckTue = (CheckBox)FindViewById(Resource.Id.ckTue);
            ckWed = (CheckBox)FindViewById(Resource.Id.ckWed);
            ckThu = (CheckBox)FindViewById(Resource.Id.ckThu);
            ckFri = (CheckBox)FindViewById(Resource.Id.ckFri);
            ckSat = (CheckBox)FindViewById(Resource.Id.ckSat);
            InitializeCheckBox();

            btnWeek = (Button)FindViewById(Resource.Id.btnWeek);
            btnWeek.Click += btnWeek_Click;

            btnMonth = (Button)FindViewById(Resource.Id.btnMonth);
            btnMonth.Click += btnMonth_Click;

            btnSaveTask = (Button)FindViewById(Resource.Id.btnSaveTask);
            btnSaveTask.Click += btnSaveTask_Click;

            txtTime = (TextView)FindViewById(Resource.Id.txtTime);
            txtTime.Text = String.Concat(DateTime.Now.Hour.ToString("00"), ":", DateTime.Now.Minute.ToString("00"));
            timeSelected = DateTime.Now.TimeOfDay;
            txtTime.Click += btnTimePicker_Click;

            txtMonth = (TextView)FindViewById(Resource.Id.txtMonth);
            txtMonth.Text = DateTime.Now.ToShortDateString();
            dateSelected = DateTime.Now;
            txtMonth.Click += txtMonth_Click;
        }

        private void btnSaveTask_Click(object sender, EventArgs e)
        {
            SQLiteModels.PersonalTask personalTask = new SQLiteModels.PersonalTask();
            
            if(actualTaskType == eTaskType.week && ValidateTask())
            {
                personalTask.Sun = ckSun.Checked;
                personalTask.Mon = ckMon.Checked;
                personalTask.Tue = ckTue.Checked;
                personalTask.Wed = ckWed.Checked;
                personalTask.Thu = ckThu.Checked;
                personalTask.Fri = ckFri.Checked;
                personalTask.Sat = ckSat.Checked;

                personalTask.description = edtDesc.Text;
                personalTask.email = Config.loggedUser.email;
                personalTask.repeat = ckRep.Checked;
                personalTask.taskDay = DateTime.MinValue;
                personalTask.taskHour = timeSelected;
            }
            if (actualTaskType == eTaskType.month)
            {
                personalTask.Sun = false;
                personalTask.Mon = false;
                personalTask.Tue = false;
                personalTask.Wed = false;
                personalTask.Thu = false;
                personalTask.Fri = false;
                personalTask.Sat = false;
                personalTask.description = edtDesc.Text;
                personalTask.email = Config.loggedUser.email;
                personalTask.repeat = false;
                personalTask.taskDay = dateSelected;
                personalTask.taskHour = timeSelected;
            }

            DatabaseManager.BODatabaseManager.CreatePersonalTask(personalTask);

            Toast.MakeText(this.ApplicationContext, "Salvo", ToastLength.Short).Show();
            this.Dispose();
        }
        
        private bool ValidateTask()
        {
            if(actualTaskType == eTaskType.week)
            {
                bool hasDayChecked = false;

                if (ckSun.Checked)
                    hasDayChecked = true;
                if (ckMon.Checked)
                    hasDayChecked = true;
                if (ckThu.Checked)
                    hasDayChecked = true;
                if (ckFri.Checked)
                    hasDayChecked = true;
                if (ckSat.Checked)
                    hasDayChecked = true;
                if (ckTue.Checked)
                    hasDayChecked = true;
                if (ckWed.Checked)
                    hasDayChecked = true;

                if (!hasDayChecked)
                {
                    Toast.MakeText(this, "Selecione um dia da semana!", ToastLength.Short).Show();
                    return false;
                }
            }

            return true;
        }

        private void btnMonth_Click(object sender, EventArgs e)
        {
            actualTaskType = eTaskType.month;
            componentMonth.Visibility = ViewStates.Visible;
            componentWeek.Visibility = ViewStates.Gone;
        }

        private void btnWeek_Click(object sender, EventArgs e)
        {
            actualTaskType = eTaskType.week;
            componentWeek.Visibility = ViewStates.Visible;
            componentMonth.Visibility = ViewStates.Gone;
        }

        private void btnTimePicker_Click(object sender, EventArgs e)
        {
            ClockFragment frag = ClockFragment.NewInstance(delegate (TimeSpan time)
            {
                timeSelected = time;
                txtTime.Text = String.Concat(time.Hours.ToString("00"), ":", time.Minutes.ToString("00"));
            });
            frag.Show(FragmentManager, ClockFragment.TAG);
        }

        private void txtMonth_Click(object sender, EventArgs e)
        {
            CalendarFragment frag = CalendarFragment.NewInstance(delegate (DateTime time)
            {
                dateSelected = time;
                txtMonth.Text = time.ToShortDateString();
            });

            frag.Show(FragmentManager, CalendarFragment.TAG);
        }

        private void InitializeCheckBox()
        {
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    ckSun.Checked = true;
                    break;
                case DayOfWeek.Monday:
                    ckMon.Checked = true;
                    break;
                case DayOfWeek.Tuesday:
                    ckTue.Checked = true;
                    break;
                case DayOfWeek.Wednesday:
                    ckWed.Checked = true;
                    break;
                case DayOfWeek.Thursday:
                    ckThu.Checked = true;
                    break;
                case DayOfWeek.Friday:
                    ckFri.Checked = true;
                    break;
                case DayOfWeek.Saturday:
                    ckSat.Checked = true;
                    break;
                default:
                    break;
            }
        }
    }
}