
using System;
using Android.App;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Widget;
using Android.Views;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using Android.Content;
using Android.Icu.Util;
using Android.Media;
using System.Threading;

namespace Iara
{
    [Activity(Label = "Tarefas Agendadas", LaunchMode = LaunchMode.SingleTop,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]

    public class RootActivity : Activity
    {
        private DrawerLayout m_Drawer;
        private ListView m_DrawerList;
        private Android.Support.V7.App.ActionBarDrawerToggle m_Toggle;
        private RecyclerView mRecyclerView;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.Adapter mAdapter;
        List<SQLiteModels.PersonalTask> mPersonalTasks;

        private static readonly string[] menuItems = new[]
        {
            "Home", "Tarefas", "Sair", "teste"
        };

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Root);

            #region Configures the action bar

            m_Drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            m_DrawerList = FindViewById<ListView>(Resource.Id.left_drawer);

            m_DrawerList.Adapter = new ArrayAdapter<string>(this, Resource.Layout.Item_Menu, menuItems);
            m_DrawerList.ItemClick += DrawerListOnItemClick;

            m_Toggle = new Android.Support.V7.App.ActionBarDrawerToggle(this, m_Drawer, Resource.String.Open, Resource.String.Close);

            m_Drawer.AddDrawerListener(m_Toggle);
            m_Toggle.SyncState();

            ActionBar.SetDisplayHomeAsUpEnabled(true);

            #endregion

            #region RecyclerView

            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.content_frame);
            mPersonalTasks = new List<SQLiteModels.PersonalTask>();

            mPersonalTasks = DatabaseManager.BODatabaseManager.GetAllPersonalTask(Config.loggedUser.email);

            //layout manager
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new RecyclerAdapter(mPersonalTasks);
            mRecyclerView.SetAdapter(mAdapter);

            MyScrollListener msc = new MyScrollListener();
            mRecyclerView.SetOnScrollListener(msc);
            #endregion
        }

        private void DrawerListOnItemClick(object sender, AdapterView.ItemClickEventArgs itemClickEventArgs)
        {
            switch (itemClickEventArgs.Position)
            {
                case 0:
                    StartAlarm(true);
                    break;
                case 1:
                    StartActivity(typeof(RegisterTaskActivity));
                    break;
                case 2:
                    System.Environment.Exit(0);
                    break;
                case 3:
                    AlarmRingtone.StopRingtone(this);
                    break;
            }

            //change the action bar of main layout
            m_DrawerList.SetItemChecked(itemClickEventArgs.Position, true);
            m_Drawer.CloseDrawer(m_DrawerList);
        }

        //Evento de click no menu
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (m_Toggle.OnOptionsItemSelected(item))
            {
                return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void StartAlarm(bool isRepeating)
        {
            AlarmManager alarm = (AlarmManager)GetSystemService(Context.AlarmService);
            Intent myIntent;
            PendingIntent pendingIntent;
            Calendar dt = Calendar.Instance;

            dt.Set(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, (DateTime.Now.Minute+1));
            
                myIntent = new Intent(this, typeof(AlarmNotificationReceiver));
                myIntent.PutExtra("extra", "on");
                pendingIntent = PendingIntent.GetBroadcast(this, 0, myIntent, PendingIntentFlags.UpdateCurrent);

            if (!isRepeating)
            {
                alarm.Set(AlarmType.RtcWakeup, DateTime.Now.AddMinutes(1).Millisecond, pendingIntent);
            }
            else
            {
                alarm.SetRepeating(AlarmType.RtcWakeup, DateTime.Now.AddMinutes(1).Millisecond, 60 * 1000, pendingIntent);
            }
        }
    }

    public class MyScrollListener : RecyclerView.OnScrollListener
    {
        //private RecyclerView.LayoutManager mLayoutManager;
        public RecyclerView.Adapter mAdapter;
        //List<SQLiteModels.PersonalTask> mPersonalTasks;

        public override void OnScrollStateChanged(RecyclerView recyclerView, int newState)
        {
        }

        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
                RecyclerAdapter controlAdapter = (RecyclerAdapter)recyclerView.GetAdapter();
                controlAdapter.UpdateData(DatabaseManager.BODatabaseManager.GetAllPersonalTask(Config.loggedUser.email));
        }
    }


    public class RecyclerAdapter : RecyclerView.Adapter
    {
        private List<SQLiteModels.PersonalTask> mPersonalTasks = new List<SQLiteModels.PersonalTask>();

        public RecyclerAdapter(List<SQLiteModels.PersonalTask> personalTasks)
        {
            mPersonalTasks = personalTasks;
        }

        public class MyView : RecyclerView.ViewHolder
        {
            public View mMainView { get; set; }
            public TextView mDesc { get; set; }
            public TextView mHour { get; set; }
            public TextView mDate { get; set; }

            public MyView(View view) : base(view)
            {
                mMainView = view;
            }
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View row = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.PersonalTask, parent, false);

            TextView txtDesc = row.FindViewById<TextView>(Resource.Id.txtDesc);
            TextView txtHour = row.FindViewById<TextView>(Resource.Id.txtHour);
            TextView txtDate = row.FindViewById<TextView>(Resource.Id.txtDate);

            MyView view = new MyView(row) { mDesc = txtDesc, mHour = txtHour, mDate = txtDate };
            return view;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            MyView myHolder = holder as MyView;
            myHolder.mDesc.Text = mPersonalTasks[position].description;
            myHolder.mHour.Text = String.Concat(mPersonalTasks[position].taskHour.Hours, ":", mPersonalTasks[position].taskHour.Minutes);
            myHolder.mDate.Text = mPersonalTasks[position].taskDay.ToShortDateString();
        }

        public override int ItemCount
        {
            get { return mPersonalTasks.Count; }
        }

        public void UpdateData(List<SQLiteModels.PersonalTask> pTasks)
        {
            mPersonalTasks.Clear();
            mPersonalTasks.AddRange(pTasks);
            NotifyDataSetChanged();
        }
    }
}