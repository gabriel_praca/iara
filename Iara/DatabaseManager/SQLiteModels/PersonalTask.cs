﻿using SQLite;
using System;
using System.Collections.Generic;

namespace SQLiteModels
{
    public class PersonalTask
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int personalTaskID { get; set; }
        public string email { get; set; }
        public TimeSpan taskHour { get; set; }
        public bool Sun { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public bool repeat { get; set; }
        public DateTime taskDay { get; set; }
        public string description { get; set; }

        public override string ToString()
        {
            return string.Format("[PersonalTask: personalTaskID={0}," +
                                                 "email={1}," +
                                                 "taskHour={2}," +
                                                 "Sun={3}," +
                                                 "Mon={4}," +
                                                 "Tue={5}," +
                                                 "Wed={6}," +
                                                 "Thu={7}," +
                                                 "Fri={8}," +
                                                 "Sat={9}," +
                                                 "repeat={10}," +
                                                 "taskDay={11}," +
                                                 "description={12}]",
                                                 personalTaskID,
                                                 email,
                                                 taskHour,
                                                 Sun,
                                                 Mon,
                                                 Tue,
                                                 Wed,
                                                 Thu,
                                                 Fri,
                                                 Sat,
                                                 repeat,
                                                 taskDay,
                                                 description);
        }
    }
}