﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManager
{
    public interface IPersonalTask
    {
        string DeletePersonalTask(string key);
        List<SQLiteModels.PersonalTask> GetAllPersonalTasks(string email);
        SQLiteModels.PersonalTask GetPersonalTask(string key);
        string SaveObject(SQLiteModels.PersonalTask task);
        string UpdateObject(SQLiteModels.PersonalTask task);
    }
}
